
def twoSum(nums: List[int], target: int) -> List[int]:
    complements = dict()
    for i,n in enumerate(nums):
        if n in complements:
            return [ complements[n], i ] 
        complements[ target - n ] = i 

    return []        

