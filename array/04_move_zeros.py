from pdb import set_trace

def moveZeroes(nums: List[int]) -> None:
    set_trace()

    size = len(nums) -1
    counter = 0

    for i in range(size, -1, -1):

        if nums[i] == 0:
            for j in range(i, counter+i):
                temp = nums[j]

                nums[j] = nums[j + 1] 
                nums[j + 1] = temp

        counter += 1
