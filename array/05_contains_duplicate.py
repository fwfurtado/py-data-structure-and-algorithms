from pdb import set_trace

def containsDuplicate(nums: List[int]) -> bool:
    cache = set()

    for n in nums:
        
        if n in cache:
            return True
        
        cache.add(n)

    return False
