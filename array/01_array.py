from pdb import set_trace


def mergeOrderedArray(a, b):

    output = []

    if a is None or not a:
        return b

    if b is None or not b:
        return a

    i = 0 
    j = 0

    item_a = a[i]
    item_b = b[j]

    while (  item_a is not None or item_b is not None ):
        if item_a is not None:
            if item_b is not None and (item_b < item_a):
                output.append(item_b)
                j += 1
            else:
                output.append(item_a)
                i += 1
        else:
            if item_b is not None:
                output.append(item_b)
                j += 1
        
        item_a = a[i] if i < len(a) else None
        item_b = b[j] if j < len(b) else None
    

    return output

    
