from pdb import set_trace
from typing import List
from sys import maxsize


def maxSubArray(nums: List[int]) -> int:

    set_trace()
    
    max_sub_array = -maxsize - 1
    current_sum = 0


    for n in nums:
        current_sum += n

        if max_sub_array < current_sum:
            max_sub_array = current_sum

        if current_sum < 0:
            current_sum = 0

    return max_sub_array

def maxSubArray_2(nums: List[int]) -> int: 

    set_trace()

    max_sub_array =  nums[0]
    current_sum = nums[0]

    for n in nums:
        current_sum = max(n, current_sum + n)
        max_sub_array = max(max_sub_array, current_sum)

    return max_sub_array
