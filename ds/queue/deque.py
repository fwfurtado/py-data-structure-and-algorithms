from doubly import LinkedList


class Deque(LinkedList):

    def peek_last(self):
        return self.last.value if self.last is not None else None

    def peek_first(self):
        return self.first.value if self.first is not None else None

    def pop_last(self):
        poped = self.last

        self.last = self.last.previous
        self.last.next = None

        return poped.value

    def pop_first(self):
        poped = self.first

        self.first = self.first.next

        return poped.value
    
    def insert_at(self, value, position):
        raise NotImplementedError("Deque can't do this operation")

    def remove_at(self, position):
        raise NotImplementedError("Deque can't do this operation")
    
