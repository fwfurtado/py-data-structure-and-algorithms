from ds.stack.node import Stack

class Queue_V1:

    def __init__(self):
        self.__stack1 = Stack()
        self.__stack2 = Stack()

        self.__head = None

    def enqueue(self, value):
        
        if self.__stack1.is_empty():
            self.__head = value

        while not self.__stack1.is_empty():
            self.__stack2.push(self.__stack1.pop())


        self.__stack2.push(value) 
        
        while not self.__stack2.is_empty():
            self.__stack1.push(self.__stack2.pop())

    def peek(self):
        return self.__head

    def dequeue(self):
        result = self.__head 

        if not self.__stack1.is_empty():
            self.__head = self.__stack1.pop()

        return self.__head

    def __repr__(self):
        return repr(self.__stack1)
        

class Queue_V2:
    def __init__(self):
        self.__stack1 = Stack()
        self.__stack2 = Stack()
        self.__head = None

    def enquene(self, value):
        if self.__stack1.is_empty():
            self.__head = value

        self.__stack1.push(value)

    def dequeue(self):
        s1 = self.__stack1
        s2 = self.__stack2

        if s2.is_empty():
            while not s1.is_empty():
                s2.push(s1.pop())
        
        result = s2.pop()

        self.__head = s2.peek()

        return result
    
    def peek(self):
        return self.__head

    def __repr__(self):
        while not self.__stack1.is_empty():
            self.__stack2.push(self.__stack1.pop())

    
        return repr(self.__stack2)

