from ds.node import Node


class Queue:

    def __init__(self, capacity=50):
        self.__max = capacity
        self.__head = None
        self.__tail = None
        self.__size = 0 

    def __len__(self):
        return self.__size

    def is_empty(self):
        return len(self) == 0 

    def is_full(self):
        return len(self) == self.__max

    def peek(self):
        if self.is_empty():
            return None

        return self.__head.value

    def enqueue(self, value):
        if self.is_full():
            raise RuntimeError('Queue is full')

        node = Node(value)
        
        if self.__head:
            self.__head = node
            self.__tail = self.__head
        else:
            node.doubly_link_with(self.__tail)
            self.__tail = node

        self.__size += 1

    def dequeue(self):
        if self.is_empty():
            return None

        result = self.__head.value
        
        if self.__head.previous:
            self.__head = self.__head.previous
            self.__head.next = None
        else:
            self.__head = None
            self.__tail = None
        
        self.__size -= 1

        return result

    def _traverse(self):
        element = self.__tail

        while element:
            yield element.value
            element = element.next

    def __repr__(self):
        
        result = ''

        for element in self._traverse():
            result += f'{element} '
        
        result = result.strip().replace(' ', '->')
        
        return result
        

