from ds.linked.doubly import LinkedList

class Queue:
    def __init__(self, capacity=50):
        self.__max = capacity
        self.__data = LinkedList()

    def __len__(self):
        return len(self.__data)

    def is_empty(self):
        return len(self) == 0

    def is_full(self):
        return len(self) == self.__max

    def peek(self):
        if self.is_empty():
            return None

        return self.__data.first.value

    def enqueue(self, value):
        if self.is_full():
            raise RuntimeError('Queue is full')

        self.__data.append(value)

    def dequeue(self):
        if self.is_empty():
            return None

        result = self.__data.first.value

        self.__data.remove_at(0)

        return result
    
    def _tranverse(self):
        element = self.__data.last

        while element:
            yield element.value
            element = element.previous

    def __repr__(self):
        result = ''

        for element in self._tranverse():
            result += f'{element} '
        
        result = result.strip().replace(' ','->')

        return result
