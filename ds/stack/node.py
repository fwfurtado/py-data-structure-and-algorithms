from ds.node import Node

class Stack:
    def __init__(self, capacity=50):
        self.__max = capacity
        self.__size = 0
        self.__top = None

    def __len__(self):
        return self.__size

    def is_empty(self):
        return len(self) == 0

    def is_full(self):
        return len(self) == self.__max

    def peek(self):
        if self.is_empty():
            return None

        return self.__top.value

    def push(self, value):

        if self.is_full():
            raise RuntimeError('Stack is full')

        node = Node(value)
        
        node.simply_link_with(self.__top)

        self.__top = node

        self.__size += 1
            
    def pop(self):
        if self.is_empty():
            return None
        
        result = self.__top.value

        self.__top = self.__top.next
        self.__size -= 1

        return result

    def _traverse(self):
        element = self.__top

        while element is not None:
            yield element.value
            element = element.next


    def __repr__(self):
        result = ''

        for element in self._traverse():
            result += f'{element}\n'

        return result 

    
    
