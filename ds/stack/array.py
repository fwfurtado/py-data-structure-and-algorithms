class Stack:
    def __init__(self, capacity=50):
        self.__capacity = capacity
        self.__data = [None] * capacity
        self.__size = 0
        self.__position = -1

    def __len__(self):
        return self.__size

    def peek(self):
        if not self.is_empty():
            return self.__data[self.__position]

        return None

    def push(self, value):
        if self.is_full():
            raise RuntimeError('Stack is full')
        
        self.__position += 1
        self.__data[self.__position] = value
        self.__size += 1

    def pop(self):
        if not self.is_empty():
            result = self.peek()
        
            self.__position -= 1
            self.__size -= 1

            return result
        
        return None

    def is_empty(self):
        return len(self) == 0

    def is_full(self):
        return len(self) == self.__capacity

    def _tranverse(self):

        for index in range(self.__position, -1, -1):
            yield self.__data[index]

    def __repr__(self):
        result = ''
        
        for element in self._tranverse():
            result += f'{element}\n'

        return result
