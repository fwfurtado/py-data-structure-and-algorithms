from ds.linked.doubly import LinkedList

class Stack:
    def __init__(self, capacity=50):
        self.__max = capacity
        self.__data = LinkedList()

    def __len__(self):
        return len(self.__data)

    def peek(self):
        node = self.__data.first
        
        return node.value if node is not None else None

    def push(self, value):
        if self.is_full():
            raise RuntimeError('Stack is full')
        
        self.__data.prepend(value)

    def pop(self):
        if not self.is_empty():
            result = self.peek()

            self.__data.remove_at(0)

            return result

        return None

    def is_full(self):
        return len(self) == self.__max

    def is_empty(self):
        return len(self) == 0
    
    def _traverse(self):
        element = self.__data.first

        while element is not None:
            yield element.value
            element = element.next

    def __repr__(self):
        result = ''

        for element in self._traverse():
            result += f'{element}\n'

        return result
