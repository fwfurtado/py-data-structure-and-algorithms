from ds.linked.bases import BaseLinkedList
from ds.node import Node


class LinkedList(BaseLinkedList):
    
    def __init__(self, value=None):
        super().__init__()

        if value is not None:
            self.append(value)
    
    def _first_insertion(self, node):
        self.first = node
        self.last = self.first

    def append(self, value):
        node = Node(value)

        if self.is_empty():
            self._first_insertion(node)
        else:
            self.last.simply_link_with(node)
            self.last = node

    def prepend(self, value):
        node = Node(value)

        if self.is_empty():
            self._first_insertion(node)
        else:
            node.simply_link_with(self.first)
            self.first = node

    def insert_at(self, value, position: int):
        
        if position >= len(self):
            self.append(value)
            return

        previous = self._traverse_to_before_of(position)
        after = previous.next

        node = Node(value)

        node.simply_link_with(after)
        previous.simply_link_with(node)

    def remove_at(self, position: int): 
        if  position < 0 or position >= len(self): 
            raise IndexError('index out of bound')

        if position == 0:
            self.first = self.first.next
            return
        
        previous = self._traverse_to_before_of(position) 
        
        target = previous.next

        after = target.next

        previous.simply_link_with(after)

        if position == len(self) - 1:
            self.last = previous
