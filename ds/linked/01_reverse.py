from simply import LinkedList

def reverse_it(linked):
    last = linked.last
    element = linked.first
    
    while element != last:
        linked.append(element.value)
        linked.remove_at(0)
        element = element.next
        

a = LinkedList()
a.append(1)
a.append(10)
a.append(16)
a.append(88)

print(a)

reverse_it(a)

print(a)
