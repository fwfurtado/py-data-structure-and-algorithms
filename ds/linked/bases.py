from abc import abstractmethod
from functools import wraps
from ds.node import Node

def _increment(func):
    
    @wraps(func)
    def inner(*args, **kwargs):
        self = args[0]
        result = func(*args, **kwargs)
        self._BaseLinkedList__size += 1

        return result 

    return inner

def _decrement(func):

    @wraps(func)
    def inner(*args, **kwargs):
        self = args[0]
        result = func(*args, **kwargs)
        self._BaseLinkedList__size -= 1

        return result
    
    return inner


class _MetaSized(type):

    def __new__(cls, class_name, bases, class_dict):
        instance = super().__new__(cls, class_name, bases, class_dict)
        
        instance.append = _increment(instance.append)
        instance.prepend = _increment(instance.prepend)
        instance.insert_at = _increment(instance.insert_at)
        instance.remove_at = _decrement(instance.remove_at)

        return instance

class BaseLinkedList(metaclass=_MetaSized):
    
    def __init__(self):
        self.__size = 0
        self.__first = None
        self.__last = None


    @abstractmethod
    def append(self, value):
        ...

    @abstractmethod
    def prepend(self, value):
        ...

    @abstractmethod
    def insert_at(self, value, position):
        ...

    @abstractmethod
    def remove_at(self, position):
        ...

    def is_empty(self):
        return self.__size == 0

    @property
    def first(self):
        return self.__first

    @first.setter
    def first(self, value: Node): 
        self.__first = value

    @property
    def last(self):
        return self.__last

    @last.setter
    def last(self, value: Node): 
        self.__last = value

    def __len__(self):
        return self.__size

    def _traverse_to(self, limit: int) -> Node:
        if limit == 0:
            return self.first
        
        element = self.first

        for _ in range(limit):
            element = element.next

        return element

    def _traverse_to_before_of(self, position):
        return self._traverse_to(position-1)

    def __repr__(self):
        result = ''
        element = self.first

        while (element is not None):
            value = element.value

            result += f'{value} '

            element = element.next
        
        result = result.strip().replace(' ', '->')

        return result
