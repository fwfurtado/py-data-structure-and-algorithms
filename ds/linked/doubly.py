from ds.linked.bases import BaseLinkedList
from ds.node import Node


class LinkedList(BaseLinkedList):

    def __init__(self, value=None):
        super().__init__()

        if value is not None:
            self.append(value)

    def _first_insertion(self, node):
        self.first = node

        self.last = self.first
    
    def prepend(self, value):
        node = Node(value) 

        if self.is_empty():
            self._first_insertion(node)

        else:
            node.doubly_link_with(self.first) 
            self.first = node

    def append(self, value):
        node = Node(value)

        if self.is_empty():
            self._first_insertion(node)

        else:
            self.last.doubly_link_with(node)

            self.last = node

    def insert_at(self, value, position: int): 
        if position > len(self):
            self.append(value)
            return
        
        target = self._traverse_to(position)
        
        previous = target.previous

        node = Node(value)

        previous.doubly_link_with(node)
        node.doubly_link_with(target)

    
    def remove_at(self, position:int):
        if position < 0 or position >= len(self):
            raise IndexError('index out of bound')

        if position == 0:
            self.first = self.first.next

            if self.first:
                self.first.previous = None

            return

        if position == len(self) - 1:
            self.last = self.last.previous
            self.last.next = None
            return
        
        target = self._traverse_to(position)

        previous = target.previous
        after = target.next

        previous.doubly_link_with(after)
