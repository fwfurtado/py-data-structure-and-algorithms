class Node:

    def __init__(self, value): 
        self.value = value
        self.next = None
        self.previous = None

    def simply_link_with(self, other: 'Node'):
        self.next = other

    def doubly_link_with(self, other: 'Node'): 
        self.next = other
        other.previous = self

    def __repr__(self):
        value = self.value
        next_element = self.next

        if next_element:
            return f'Node({value}, {next_element.value})'

        return f'Node({value}, None)'
