from typing import List
from pdb import set_trace

class HashSet:

    def __init__(self, size=50):
        self.__data = [None] * size
        self.__size = size

    def add(self, value):
        address = self._hash_it(value)

        self.__data[address] = value

    def exist(self, value):
        address = self._hash_it(value)

        return self.__data[address] is not None

    def remove(self, value):
        address = self._hash_it(value)

        if self.__data[address] is not None: 
            self.__data[address] = None

            return True

        return False

    def _hash_it(self, value):
        size = self.__size
        current_hash = 0 
        
        current_value = value
        counter = 1

        while current_value > 0: 

            num = round(current_value / 10 % 1 * 10)

            current_hash = (current_hash + num * counter) % size

            current_value = int(current_value / 10)

            counter += 1

        return current_hash
    

def has_duplicates(values):
    elements = HashSet()

    for value in values:
        if elements.contains(value):
            return True

        elements.add(value)

    return False
