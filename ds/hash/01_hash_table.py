from typing import List
from pdb import set_trace

class HashTable:

    def __init__(self, size=50):
        self.__data = [None] * size 
        self.__keys = []

    
    def _find(self, searched_key):
        address = self._hash_it(searched_key)
        buckets = self.__data[address] 

        if buckets is not None: 
            for index in range(0, len(buckets)):
                if buckets[index] is not None:
                    key = buckets[index][0]
                    if key == searched_key:
                        return address, index

        return address, None        

    def keys(self):
        return self.__keys

    def get(self, searched_key):
        address, bucket_index = self._find(searched_key)

        if bucket_index:
            self.__data[address][bucket_index]

    def set(self, key, value):
        new_bucket = (key, value)
        address, bucket_index = self._find(key)

        if bucket_index is not None:
            self.__data[address][bucket_index] = new_bucket
        else: 
            self.__keys.append(key)
            if self.__data[address]:
                self.__data[address].append(new_bucket)
            else:
                self.__data[address] = [new_bucket]

    def _hash_it(self, key: str) -> int: 
        _hash = 0
        size = len(self.__data) 

        for index, letter in enumerate(key):
            _hash = ( _hash + ord(letter) * index ) % size

        return _hash
