from ds.trees.utils import BinaryNode

class Tree:
    def __init__(self):
        self.__root = None
    def is_empty(self) -> bool:
        return self.__root is None
    

    def insert(self, value):
        
        new_node = BinaryNode(value)

        if self.is_empty():
            self.__root = new_node
            return

        current = self.__root

        while current:

            if current == new_node:
                return False
            
            if new_node < current:
                if not current.left:
                    current.left = new_node
                    return True

                current = current.left

            if new_node > current:
                if not current.right:
                    current.right = new_node
                    return True

                current = current.right

        return False


    def lookup(self, value):
        target_node = BinaryNode(value)
        current = self.__root

        if current:
            while current:

                if current == target_node:
                    return current

                if current > target_node:
                    current = current.left
                    continue

                if current < target_node:
                    current = current.right
        return False
    
    def remove(self, value) -> bool:
        if self.is_emtpy():
            return False

        target_node = BinaryNode(value)
        current = self.__root
        parent = None

        while current:
            
            if target_node > current:
                parent = current
                current = current.left
                continue

            if target_node < current:
                parent = current
                current = current.right
                continue

            if current == target_node:
                if not current.right:
                    if not parent:
                        self.__root = current.left
                    else:
                        if parent < current:
                            parent.left = curent.left
                        else:
                            parent.right = current.left

                    return True
            
                if not current.right.left:
                    if not parrent:
                        self.__root = current.left
                    else:
                        current.right.left = current.left

                        if parent > current:
                            parent.left = current.right
                        else:
                            parent.right = current.right
                    
                    return True

                if current.right.left:
                    left_most = current.right.left
                    left_most_parent = current.right

                    while left_most.left:
                        left_most_parent = left_most
                        left_most = left_most.left

                    left_most_parent.left = left_most.right
                    left_most.left = current.left
                    left_most.right = current.right

                    if not parent:
                        self.__root = left_most
                    else:
                        if parent > current:
                            parent.left = left_most
                        else:
                            parent.right = left_most
                    return True

        return False
