
class BinaryNode:

    def __init__(self, value):
        self.__value = value
        self.__left = None
        self.__right = None

    @property
    def value(self):
        return self.__value

    def __gt__(self, other: 'BinaryNode'):
        return self.value > other.value

    def __ge__(self, other: 'BinaryNode'):
        return self.value >= other.value

    def __lt__(self, other: 'BinaryNode'):
        return self.value < other.value

    def __le__(self, other: 'BinaryNode'): 
        return self.value <= other.value

    def __eq__(self, other: 'BinaryNode'):
        return self.value == other.value

    def __ne__(self, other: 'BinaryNode'): 
        return not self == other

    def has_next(self):
        return self.left is not None or self.right is not None
    
    @property
    def left(self):
        return self.__left
    
    @left.setter
    def left(self, value):
        self.__left = value
    
    @property
    def right(self):
        return self.__right

    @right.setter
    def right(self, value):
        self.__right = value

    def __repr__(self):
        result = ''

        if self.__left:
            result += f'{self.__left.value} <- '

        result += f'{self.value}'

        if self.__right:
            result += f' -> {self.__right.value}'

        return result
